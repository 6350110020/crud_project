import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'product.dart';
import 'update.dart';

class Details extends StatefulWidget {
  final Products products;

  Details({required this.products});

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  void deleteProducts(context) async {
    await http.post(
      Uri.parse("http://10.0.2.2/android/delete_product.php"),
      body: {
        'pid': widget.products.pid.toString(),
      },
    );
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  void confirmDelete(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title:  Text('Are you sure you want to delete this?'),

          actions: <Widget>[
            ElevatedButton.icon(
              label: Text('No'),
              icon: Icon(Icons.cancel),
              style: ElevatedButton.styleFrom(
                primary: Colors.deepOrange,
                onPrimary: Colors.white,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
            ElevatedButton.icon(
              label: Text('Yes'),
              icon: Icon(Icons.check_circle),
              style: ElevatedButton.styleFrom(
                primary: Colors.indigo,
                onPrimary: Colors.white,
              ),
              onPressed: () => deleteProducts(context),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Products Detail'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.backspace),
            onPressed: () => confirmDelete(context),
          ),
        ],
      ),
      backgroundColor: Colors.orange[100],
      body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Center(
                  child: Image.asset('assets/images/4.png',height: 260,),
                ),
              ),
              Text(
                  "Name : ${widget.products.name}",
                  style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10,bottom: 10),
                child: Text(
                  "Price : ${widget.products.price}",
                  style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                "Description : ${widget.products.description}",
                style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),

      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.mode_edit_outlined),
        onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => Edit(products: widget.products),
          ),
        ),
      ),
    );
  }
}

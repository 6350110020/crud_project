import 'package:crudmysql_api/main.dart';
import 'package:flutter/material.dart';

import 'about.dart';

class Gridview1 extends StatefulWidget {
  const Gridview1({Key? key}) : super(key: key);

  @override
  State<Gridview1> createState() => _Gridview1State();
}

class _Gridview1State extends State<Gridview1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Gallery'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home),
            onPressed: (){
              Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => Home(),
                  )
              );
            },
          ),
        ],
      ),
      backgroundColor: Colors.orange[100],
      body: Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 3),
        child: Center(
          child: GridView.count(
              primary: false,
              padding: const EdgeInsets.all(10),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 2,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      new Image.asset('assets/images/ja5.jpg',
                        fit: BoxFit.cover,
                        height: 180,
                        width: 200,
                        alignment: Alignment.center,
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      new Image.asset('assets/images/ja6.jpg',
                        fit: BoxFit.cover,
                        height: 180,
                        width: double.infinity,
                        alignment: Alignment.center,
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      new Image.asset('assets/images/ja2.jpg',
                        fit: BoxFit.cover,
                        height: 180,
                        width: double.infinity,
                        alignment: Alignment.center,
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      new Image.asset('assets/images/ja3.jpg',
                        fit: BoxFit.cover,
                        height: 180,
                        width: double.infinity,
                        alignment: Alignment.center,
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      new Image.asset('assets/images/ja4.jpg',
                        fit: BoxFit.cover,
                        height: 180,
                        width: double.infinity,
                        alignment: Alignment.center,
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      new Image.asset('assets/images/ja1.jpg',
                        fit: BoxFit.cover,
                        height: 180,
                        width: double.infinity,
                        alignment: Alignment.center,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(child: Text("."),
              margin: EdgeInsets.zero,
              padding: EdgeInsets.zero,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image:  AssetImage('assets/images/1.png'))),
            ),
            ListTile(
              leading: Icon(Icons.home,size: 30,color: Colors.black,),
              title: Text("Home",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500),),
              onTap: (){
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Home(),
                    )
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.person,size: 30,color: Colors.black,),
              title: Text("About",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500),),
              onTap: (){
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => About(title: '',),
                    )
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.image,size: 30,color: Colors.black,),
              title: Text("Gallery",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500),),
              onTap: (){
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Gridview1(),
                    )
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

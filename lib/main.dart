import 'package:crudmysql_api/gridview.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'about.dart';
import 'product.dart';
import 'details.dart';
import 'insert.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  late Future<List<Products>> products;

  @override
  void initState() {
    super.initState();
    products = getProductsList();
  }

  Future<List<Products>> getProductsList() async {
    String url = "http://10.0.2.2/android/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return productsFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load products');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Products list'),
      ),
      backgroundColor: Colors.orange[100],
      body: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Center(
          child: FutureBuilder<List<Products>>(
            future: products,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              // By default, show a loading spinner.
              if (!snapshot.hasData) return CircularProgressIndicator();
              // Render Products lists
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  var data = snapshot.data[index];
                  return Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                    ),
                    child: ListTile(
                      leading: Icon(Icons.shopify,color: Colors.deepOrange,size: 35,),
                      trailing: Icon(Icons.segment_outlined,color: Colors.black,),
                      subtitle: Text(data.price),
                      title: Text(
                        data.name,
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                      ),

                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Details(products: data)),
                        );
                      },
                    ),
                  );
                },
              );
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add,color: Colors.white,),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Insert()),
          );
        },
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(child: Text("."),
              margin: EdgeInsets.zero,
              padding: EdgeInsets.zero,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image:  AssetImage('assets/images/1.png'))),
              ),
            ListTile(
              leading: Icon(Icons.home,size: 30,color: Colors.black,),
              title: Text("Home",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500),),
              onTap: (){
                Navigator.of(context).pop();
              },
            ),
            ListTile(
              leading: Icon(Icons.person,size: 30,color: Colors.black,),
              title: Text("About",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500),),
              onTap: (){
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => About(title: '',),
                    )
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.image,size: 30,color: Colors.black,),
              title: Text("Gallery",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500),),
              onTap: (){
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Gridview1(),
                    )
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

